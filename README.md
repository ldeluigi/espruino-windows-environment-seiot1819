# ISTRUZIONI #

* Clona il repo
* Spostati nella cartella del repo con un interprete comandi Windows (batch, cmd, PowerShell)
* lancia setup.bat
```
git clone https://ldeluigi@bitbucket.org/ldeluigi/espruino-windows-environment-seiot1819.git
cd espruino-windows-environment-seiot1819
.\ setup.bat
```

## Uso dell'environment ##

Consultare il [.pdf](https://bitbucket.org/ldeluigi/espruino-windows-environment-seiot1819/raw/bd3116029e5422d88d35dd811d194179497a95ae/Note-sui-Sistemi-ESP8266&NodeMCU.pdf) e seguire le istruzioni tenendo presente le differenze tra un ambiente Windows e linux.
I file per cancellare la flash e per installare il S.O. sono erase.bat e flash.bat (sostituti di erase.py e flash.py).

Per eseguire file javascript rivolgersi alla guida di espruino (contenuta nel pdf oppure [qui](https://www.espruino.com/Programming#espruino-command-line-tool))
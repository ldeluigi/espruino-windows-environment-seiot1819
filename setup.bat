@ECHO OFF
:BEGIN
git submodule init esptool
git submodule update esptool
CHOICE /C YNC /T 20 /D N /M "Do you want to clone and pull updates from seiot1819 repository? [Yes, No, Cancel]"
IF ERRORLEVEL 3 GOTO CANC
IF ERRORLEVEL 2 GOTO NOSEIOT
IF ERRORLEVEL 1 GOTO YESSEIOT
IF ERRORLEVEL 0 GOTO CANC
:YESSEIOT
git submodule init seiot1819 && git submodule update --remote seiot1819
:NOSEIOT
WHERE >nul 2>nul python
IF %ERRORLEVEL% NEQ 0 ( ECHO "python wasn't found. Install at https://www.python.org/downloads/release/python-2715/" & GOTO END )
WHERE >nul 2>nul pip
IF %ERRORLEVEL% NEQ 0 ( ECHO "python-pip wasn't found. Install at https://pypi.org/project/pip/" & GOTO END )
IF NOT EXIST ".\esptool\esptool.py" ( ECHO ".\esptool\esptool.py not found. Please check environment setup." & GOTO END )
WHERE >nul 2>nul espruino
IF %ERRORLEVEL% NEQ 0 ( ECHO "espruino wasn't found. Install with \"npm install -g espruino\" from npm. You can find npm with Node.js at https://nodejs.org/en/" & GOTO END )
pip install pyserial --upgrade
ECHO Setup done. Available devices: espruino --list (Exception if none is found)
ECHO If the port isn't COM12 set the port inside erase.bat and flash.bat by adding --port PORTNAME to the commands
ECHO Try .\erase.bat and .\flash.bat
ECHO If the commands throw a Bluetooth related exception use espruino with the --no-ble flag
ECHO More info at https://www.npmjs.com/package/espruino
GOTO NOCANCEL
:CANC
ECHO "Canceled"
:NOCANCEL
:END
pause
